<?php

function displayThumb($nbArticle){
	
	$dirImg = "img/voyage/".$nbArticle."/img/";
	$dirThumb = "img/voyage/".$nbArticle."/thumb/";



	$chaine="[";

	$list=scandir($dirImg);

	foreach($list as $file){
			if($file != '.' && $file != '..' && !is_dir($dirImg.$file)){
				$chaine.="'".$dirImg.$file."',";
			}
	}
	
	$chaine=substr($chaine,0,strlen($chaine)-1);
	$chaine.="]";


	$nb=0;
	$list=scandir($dirThumb);
	

	foreach($list as $file){
		if($file != '.' && $file != '..' && !is_dir($dirThumb.$file)){
		
			echo "<div class=\"col-md-3 col-sm-6 col-xs-6\">
								<div class=\"service-item animated\">
								<img src=\"".$dirThumb.$file."\" onClick=\"show_imageviewer(".$chaine.",".$nb.");\" />
								</div>
							</div>";
		
			$nb=$nb+1;
		}
	}
}	



function displaySummary($dir){

	$nb=0;
	
	$list=scandir($dir);

	foreach($list as $file){
			if($file != '.' && $file != '..' && !is_dir($dir.$file)){
				
				echo "<div class=\"col-md-3 col-sm-6\">
								<div class=\"thumbnail team-profile\">
								<a href=\"#.info".$nb."\" class=\"anchorLink\"
									<h5>".substr($file,2,strlen($file))."</h5>
									</a>
								</div>
							</div>";	
				$nb=$nb+1;
			}
	}
}

function displayArticle($dir){	
	$nb=0;
	
	$list=scandir($dir);

	foreach($list as $file){
			if($file != '.' && $file != '..' && !is_dir($dir.$file)){
				echo "<div class=\"service\">
						<div class=\"info".$nb."\"></div>
							<div class=\"bor hidden-xs\"></div>	
								<h3>".substr($file,2,strlen($file))."</h3>		
								";
								include($dir.'/'.$file);
					echo "</div>	
					<hr />";			
						
				$nb=$nb+1;
			}
	}
}

//displaySummary('info');

?>