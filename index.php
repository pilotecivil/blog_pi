			<?php  include('site_tete.html')?>
			<div class="main">
			
				<div id="carousel-example-generic" class="carousel slide carousel-fade">
				   
					<!-- Wrapper for slides -->
					
					<div class="carousel-inner">
						<!-- Carousel item start -->
						
						<div class="item active">
							<!-- Carousel background images -->
							<img src="img/fond_index.JPG" class="img-responsive" alt="" />
							<div class="carousel-caption">
								<div class="row">
									<div class="col-md-6">
										<!-- Images for carousel foreground -->
										<img src="img/index.png" alt="" class="img-responsive" />
									</div>
									<div class="col-md-6">
										<!-- Carousel caption -->
										<div class="caption-content">
											<h3>Antoine Courcelles</h3>
											<p>
											Bienvenue sur mon site auto-hébergé !<br> Au programme récit de voyage, astuce d'info ou simplement des bêtises !
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
						
						
			
					</div>
					</div>
				
				<!-- Wrapper for slides End -->
				
				
				
				
					<hr />

				<footer>
					<div class="row">
						<div class="col-md-12">
							<hr />
							<br />
							<!-- You should not remove the footer link back. -->
							<p class="text-center"><a href="index.html">Antoine Courcelles</a> - Designed by <a href="http://responsivewebinc.com/bootstrap-themes">Bootstrap Themes</a></p>
							<br />
						</div>
					</div>
				</footer>
				
			</div>
			<?php  include('site_fin.html')?>
