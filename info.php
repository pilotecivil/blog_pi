
			<?php  include('site_tete.html')?>
			<div class="main">
			
				<div id="carousel-example-generic" class="carousel slide carousel-fade">
				   

					
					<div class="carousel-inner">
						<!-- Carousel item start -->
						<div class="item active">
							<!-- Carousel background images -->
							<img src="img/fond_info.jpg" class="img-responsive" alt="" />
							<div class="carousel-caption">
								<div class="row">
									<div class="col-md-6">
										<!-- Images for carousel foreground -->
										<img src="img/pi.png" alt="" class="img-responsive" />
									</div>
									<div class="col-md-6">
										<!-- Carousel caption -->
										<div class="caption-content">
											<h3>Informatique</h3>
											Logiciel libre,<br> Auto-hébergement,<br> astuce, c'est ici :)
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				
				<div class="container">
				
					<div class="team">
						<h3>Sommaire</h3>
						<div class="bor hidden-xs"></div>
							<div class="row">
								<?php displaySummary('info'); ?>
							</div>	
					</div>
					<hr />					
					<?php displayArticle('info'); ?>			
				</div>	
								<footer>
					<div class="row">
						<div class="col-md-12">
							<hr />
							<br />
							<!-- You should not remove the footer link back. -->
							<p class="text-center"><a href="#">Antoine Courcelles</a> - Designed by <a href="http://responsivewebinc.com/bootstrap-themes">Bootstrap Themes</a></p>
							<br />
						</div>
					</div>
				</footer>
				
				</div>

				
			<?php  include('site_fin.html')?>
	