
			<?php  include('site_tete.html')?>
			
			<div class="main">
			
				<div id="carousel-example-generic" class="carousel slide carousel-fade">
				   
					<!-- Wrapper for slides -->
					
					<div class="carousel-inner">
						<!-- Carousel item start -->
						<div class="item active">
							<!-- Carousel background images -->
							<img src="img/monde.jpg" class="img-responsive" alt="" />
							<div class="carousel-caption">
								<div class="row">
									<div class="col-md-6">
										<!-- Images for carousel foreground -->
										<img src="img/haut.jpg" alt="" class="img-responsive" />
									</div>
									<div class="col-md-6">
										<!-- Carousel caption -->
										<div class="caption-content">
											<h3>Amérique du Sud</h3>
											<p>
											15 jours du 19 avril au 2 mai 2014,<br> 27270 km,<br>deux pays visités,<br> voici l'Amérique du Sud !
											</p>
										</div>
									</div>
								</div>
							</div>
						</div>
				  
						<div class="item">
							<img src="img/monde.jpg"  class="img-responsive" alt=""/>
							<div class="carousel-caption">
								<div class="row">
									<div class="col-md-6">
										<img src="img/uru.jpg" alt="" class="img-responsive" />
									</div>
									<div class="col-md-6">
										<div class="caption-content">
											<h3>L'Uruguay</h3>
											<p>Son maté et sa tranquillité !</p>
										</div>
									</div>
								</div>
							</div>
						</div>
				  
						<div class="item">
							<img src="img/monde.jpg"  class="img-responsive" alt=""/>
							<div class="carousel-caption">
								<div class="row">
									<div class="col-md-6">
										<img src="img/fitz.jpg" alt="" class="img-responsive" />
									</div>
									<div class="col-md-6">
										<div class="caption-content">
											<h3>La Patagonie</h3>
											<p>Son climat et ses paysages !</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

					<!-- Controls -->

					<a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
						<span class="icon-prev"></span>
					</a>
					<a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
						<span class="icon-next"></span>
					</a>
				</div>
				
				<!-- Wrapper for slides End -->
				
				
				
				<div class="container">
				
					<div class="team">
						<h3>Les étapes</h3>
						<br>
						<div class="row">
						
							<div class="col-md-3 col-sm-6">
								<!-- Team member's profile  -->
								<div class="thumbnail team-profile">
								<a href="#.trav" class="anchorLink">
									<img src="img/b777.jpg" alt="" class="img-responsive img-circle" />
									</a>
									<!-- Member's name and designation -->
									<h5>Traversée de l'Atlantique</h5>
									<span>Triple 7</span>
								</div>
							</div>
							
							<div class="col-md-3 col-sm-6">
								<!-- Team member's profile  -->
								<div class="thumbnail team-profile">
								<a href="#.uru" class="anchorLink">
									<img src="img/archi.JPG" alt="" class="img-responsive img-circle" />
									</a>
									<!-- Member's name and designation -->
									<h5>Uruguay</h5>
									<span>Echec et maté</span>
								</div>
							</div>
							
							<div class="col-md-3 col-sm-6">
								<!-- Team member's profile  -->
								<div class="thumbnail team-profile">
								<a href="#.pata1" class="anchorLink">
									<img src="img/pata.jpg" alt="" class="img-responsive img-circle" />
									</a>
									<!-- Member's name and designation -->
									<h5>Patagonie</h5>
									<a href="#.pata1" class="anchorLink">
									<span>Patagon, nous étions</span>
									</a>
									<a href="#.pata2" class="anchorLink">
									<span>Patagon, nous avons failli rester</span>	
									</a>
								</div>
							</div>
										
							<div class="col-md-3 col-sm-6">
								<!-- Team member's profile  -->
								<div class="thumbnail team-profile">
								<a href="#.retour" class="anchorLink">
									<img src="img/retour.jpg" alt="" class="img-responsive img-circle" />
									</a>
									<!-- Member's name and designation -->
									<h5>Retour</h5>
									<span>Last call for Sao Paulo</span>
									<span> </span>
								</div>
							</div>
				
				
				
				
						</div>	
					</div>
				
					<div class="contact">
					<div class="bor hidden-xs"></div>
						<h3>Itinéraire</h3><br>
						<iframe src="https://mapsengine.google.com/map/embed?mid=ztwndwFEeKV0.k0zzD1yNXyts" width="800" height="480"></iframe>
					</div>
					<hr />				

					<div class="service">
						<div class="trav"></div>
							<div class="bor hidden-xs"></div>	
								<h3>J'ai traversé l'atlantique</h3>		
								<?php include('voyage/1.html'); ?>
						<br />	
						
								<h3>Quelques photos</h3>		
						<br />	
						<div class="row">
							<?php displayThumb(1); ?>
							
						</div>
					</div>	
					
					<hr />
					<div class="service">
						<div class="uru"></div>
							<div class="bor hidden-xs"></div>	
								<h3>Uruguay, Echec et maté</h3>				
								<?php include('voyage/2.html'); ?>
								<br />	
						
								<h3>Quelques photos</h3>		
						<br />	
						<div class="row">
							<?php displayThumb(2); ?>
							
						</div>	
					</div>	
					<hr />
					
					<div class="service">
						<div class="pata1"></div>
							<div class="bor hidden-xs"></div>
								<h3>Patagon, nous étions</h3>
								<?php include('voyage/3.html'); ?>
								<br />	
						
								<h3>Quelques photos</h3>		
						<br />	
						<div class="row">
							<?php displayThumb(3); ?>
							
						</div>	
					</div>						
					<hr />
					
					<div class="service">
						<div class="pata2"></div>
							<div class="bor hidden-xs"></div>
								<h3>Patagon, nous avons failli rester</h3>
								<?php include('voyage/4.html'); ?>
									<br />	
						
								<h3>Quelques photos</h3>		
						<br />	
						<div class="row">
							<?php displayThumb(4); ?>
							
						</div>	
					</div>										
					<hr />
					
					<div class="service">
						<div class="retour"></div>
							<div class="bor hidden-xs"></div>
								<h3>Last Call for Sao Paulo</h3>
			
					</div>	
										
				</div>
				<hr />

				<footer>
					<div class="row">
						<div class="col-md-12">
							<hr />
							<br />
							<!-- You should not remove the footer link back. -->
							<p class="text-center"><a href="index.html">Antoine Courcelles</a> - Designed by <a href="http://responsivewebinc.com/bootstrap-themes">Bootstrap Themes</a></p>
							<br />
						</div>
					</div>
				</footer>
				
				</div>
				
								
				<?php  include('site_fin.html')?>